// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * Description: Binary managing Videos, using libmpv
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Date:        $Date$
 *****************************************************************************/

#include "iVideosPlayer.h"

#include <string.h>
#include <syslog.h>

sConf configuration;

int main (int argc, char ** argv)
{
    // Configure Logs
    if (configureLogs ())
        cleanExit (EXIT_FAILURE);

    // Initialisation of default parameters
    if (initConfiguration () != SUCCESS)
        cleanExit (EXIT_FAILURE);

    // Override default parameters with command line arguments
    if (parseParameters (argc, argv) != SUCCESS)
        cleanExit (EXIT_FAILURE);

    logDebug ("Configuration:");
    logDebug ("- File: %s", configuration.file);

    if (writePidFile (configuration.pidfile) != SUCCESS)
        cleanExit (EXIT_FAILURE);

    // Execute command
    if (strlen (configuration.file))
    {
        logDebug ("Load file: %s", configuration.file);
        // Load given file
        if (playerLoad (configuration.file) != SUCCESS)
            cleanExit (EXIT_FAILURE);
    }

    cleanExit (EXIT_SUCCESS);
}

int cleanExit (int returnValue)
{
    char fullpidfile [PATH_MAX_LENGTH];
    memset (fullpidfile, '\0', sizeof (fullpidfile));

    logDebug ("Exiting [%d]", returnValue);
    if (configuration.ctx)
    {
        mpv_terminate_destroy (configuration.ctx);
        configuration.ctx = NULL;
    }
    if (configuration.pidfile)
    {
        if (configuration.pidfile [0] == '/')
            strncpy (fullpidfile, configuration.pidfile,
                        sizeof (fullpidfile) - 1);
        else
        {
            snprintf (fullpidfile, sizeof (fullpidfile) - 1,
                        "%s/%s", DEFAULT_PID_PATH, configuration.pidfile);
        }
        logDebug ("Removing PID file '%s'", fullpidfile);
        unlink (fullpidfile);
    }
    closelog ();
    exit (returnValue);
}

// Configure Syslog messages
int configureLogs ()
{
    // Allow any messages, even Debug, to be sent to Syslog
    setlogmask (LOG_UPTO (LOG_DEBUG));

    // Configure logs facilities
    openlog (PLAYERNAME, LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

    logDebug ("Connected to Syslog.");
    return (SUCCESS);
}

void logDebug (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL1|LOG_DEBUG, message, ap);
    va_end (ap);
}

void logInfo (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL1|LOG_INFO, message, ap);
    va_end (ap);
}

void logError (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL1|LOG_ERR, message, ap);
    va_end (ap);
}

// Initialize default configuratino for the Videos Player
int initConfiguration ()
{
    // Initialize configuration structure
    memset (configuration.pidfile,  '\0', PATH_MAX_LENGTH);
    memset (configuration.file,     '\0', PATH_MAX_LENGTH);
    memset (configuration.aspect,   '\0', ASPECT_MAX_LENGTH);
    memset (configuration.length,   '\0', TIME_MAX_LENGTH);
    memset (configuration.start,    '\0', TIME_MAX_LENGTH);
    memset (configuration.volume,   '\0', VOLUME_MAX_LENGTH);

    configuration.pause     = 0;
    configuration.render    = RENDER_DEFAULT;

    configuration.x         = -1;
    configuration.y         = -1;
    configuration.height    = -1;
    configuration.width     = -1;

    configuration.ctx       = NULL;
    configuration.gStop     = 0;

    return (SUCCESS);
}

// Parse parameters given in command line, to override default configuration
int parseParameters (int argc, char ** argv)
{
    int c, option_index = 0;

    // Global info are updated with parameters of the command line
    while ((c = 
        getopt_long (argc, argv, "f:hl:ps:yz", long_options, &option_index)) != EOF)
    {
        switch (c)
        {
            case 'a':
                // Aspect of the video (scale, keep and cut, keep and resize)
                strncpy (configuration.aspect, optarg, ASPECT_MAX_LENGTH-1);
                break;
            case 'f':
                // File to play
                strncpy (configuration.file, optarg, PATH_MAX_LENGTH-1);
                break;
            case 'h':
                // Display help
                fprintf (stdout, "%s\n", HELP);
                cleanExit (SUCCESS);
            case 'l':
                // Stop after X seconds
                strncpy (configuration.length, optarg, TIME_MAX_LENGTH-1);
                break;
            case 'P':
                // PID file
                strncpy (configuration.pidfile, optarg, PATH_MAX_LENGTH-1);
                break;
            case 'p':
                // Start the video in 'pause'
                configuration.pause = 1;
                break;
            case 's':
                // Seek to given time position
                strncpy (configuration.start, optarg, TIME_MAX_LENGTH-1);
                break;
            case 'v':
                // Volume
                strncpy (configuration.volume, optarg, VOLUME_MAX_LENGTH-1);
                break;
            case 'y':
                // Use VAAPI
                configuration.render = RENDER_VAAPI;
                break;
            case 'z':
                // Use VDPAU
                configuration.render = RENDER_VDPAU;
                break;
            case 'X':
                configuration.x = atoi (optarg);
                break;
            case 'Y':
                configuration.y = atoi (optarg);
                break;
            case 'H':
                configuration.height = atoi (optarg);
                break;
            case 'W':
                configuration.width = atoi (optarg);
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return (FAILURE);
        }

        // Reinit the option_index
        option_index = 0;
    }

    return (0);
}

int writePidFile (char * path)
{
    FILE * f = NULL;
    pid_t current_pid = 0;
    char fullPidFile [PATH_MAX_LENGTH];
    memset (fullPidFile, '\0', sizeof (fullPidFile));

    // Nothing to do ?
    if (!strlen (path))
        return (SUCCESS);

    if (path [0] != '/')
    {
        // Relative path ? (means /var/run/iVideosPlayer/xxxx)
        snprintf (fullPidFile, sizeof (fullPidFile) - 1, "%s/%s", 
                    DEFAULT_PID_PATH, path);
    }
    else
    {
        snprintf (fullPidFile, sizeof (fullPidFile) - 1, "%s", path);
    }

    // Write current PID to this file
    f = fopen (fullPidFile, "w");
    if (!f)
    {
        logError ("Cannot write PID in file '%s'.", fullPidFile);
        return (FAILURE);
    }
    
    current_pid = getpid ();
    logDebug ("Writing current PID [%d] to file '%s'.", current_pid, fullPidFile);
    fprintf (f, "%d", current_pid);
    fclose (f);
    return (SUCCESS);
}

int playerLoad (char * path)
{
    int status;

    // Create mpv context
    configuration.ctx = mpv_create ();
    if (!configuration.ctx)
    {
        logError ("Cannot create mpv context.");
        return (FAILURE);
    }
    
    // Set options
    status = playerSetInitOptions (configuration.ctx);
    if (status != SUCCESS)
        return (FAILURE);

    // Done setting up options
    status = mpv_initialize (configuration.ctx);
    if (status < 0)
    {
        logError ("mpv_initialize: %s", mpv_error_string (status));
        return (FAILURE);
    }

    // Load the file
    const char *cmd[] = {"loadfile", path, NULL};
    status = mpv_command (configuration.ctx, cmd);
    if (status < 0)
    {
        logError ("mpv_command: %s", mpv_error_string (status));
        return (FAILURE);
    }

    // Signal handlers
    signal (SIGUSR1, sigPlay);
    signal (SIGUSR2, sigPause);

    // Wait the file to be played
    while (configuration.gStop == 0)
    {
        mpv_event *event = mpv_wait_event (configuration.ctx, 1);
        if (event->event_id != MPV_EVENT_NONE)
            logDebug ("event: %s\n", mpv_event_name (event->event_id));
        if (event->event_id == MPV_EVENT_SHUTDOWN)
            break;
    }

    return (SUCCESS);
}

int playerSetInitOptions (mpv_handle * ctx)
{
    char geometry [32];
    memset (geometry, '\0', sizeof (geometry));

    // - Quit when video is ended
    if (playerSetMpvOption (ctx, "idle", "once")) return (FAILURE);

    // - No interactions with terminal, on-screen controller
    if (playerSetMpvOption (ctx, "terminal", "no")) return (FAILURE);
    if (playerSetMpvOption (ctx, "osc", "no")) return (FAILURE);

    // - No Youtube downloader
    if (playerSetMpvOption (ctx, "ytdl", "no")) return (FAILURE);

    // - No Borders
    if (playerSetMpvOption (ctx, "border", "no")) return (FAILURE);

    // - Volume to 0
    if (playerSetMpvOption (ctx, "volume", "0")) return (FAILURE);

    // - Pause
    if (configuration.pause)
        if (playerSetMpvOption (ctx, "pause", "yes")) return (FAILURE);

    // - Seek to given time position
    if (strlen (configuration.start))
        if (playerSetMpvOption (ctx, "start", configuration.start)) return (FAILURE);
    // - Length of the video to play
    if (strlen (configuration.length))
        if (playerSetMpvOption (ctx, "length", configuration.length)) return (FAILURE);

    switch (configuration.render)
    {
        case RENDER_VAAPI:
            if (playerSetMpvOption (ctx, "hwdec", "vaapi")) return (FAILURE);
            break;
        case RENDER_VDPAU:
            if (playerSetMpvOption (ctx, "hwdec", "vdpau")) return (FAILURE);
            break;
        case RENDER_DEFAULT:
        default:
            // Best HW accel found
            if (playerSetMpvOption (ctx, "hwdec", "auto")) return (FAILURE);
            break;
    }

    if (strlen (configuration.aspect))
    {
        if (strncmp (configuration.aspect, "scale", ASPECT_MAX_LENGTH) == 0)
        {
            if (playerSetMpvOption (ctx, "keepaspect", "no")) return (FAILURE);
        }
        else if (strncmp (configuration.aspect, "keep-crop", ASPECT_MAX_LENGTH) == 0)
        {
            if (playerSetMpvOption (ctx, "video-unscaled", "yes")) return (FAILURE);
        }
        else if (strncmp (configuration.aspect, "keep-resize", ASPECT_MAX_LENGTH) == 0)
        {
            if (playerSetMpvOption (ctx, "video-unscaled", "downscale-big")) return (FAILURE);
        }
        else
        {
            // Default is to scale Video
            logError ("Unknown aspect option: '%s'. Setting it to 'scale'.",
                        configuration.aspect);
            if (playerSetMpvOption (ctx, "no-keepaspect", "yes")) return (FAILURE);
        }
    }

    // If any window component (position or size) is given in parameter,
    // specify the window's size and position
    if (configuration.x >= 0 || configuration.y >= 0 ||
        configuration.width >= 0 || configuration.height >= 0)
    {
        if (configuration.x >= 0 && configuration.y >= 0)
        {
            snprintf (geometry, sizeof (geometry), "%dx%d+%d+%d",
                        configuration.width,
                        configuration.height,
                        configuration.x,
                        configuration.y);
        }
        else
        {
            snprintf (geometry, sizeof (geometry), "%dx%d",
                        configuration.width,
                        configuration.height);
        }
        if (playerSetMpvOption (ctx, "geometry", geometry)) return (FAILURE);
    }

    return (SUCCESS);
}

int playerSetMpvOption (mpv_handle * ctx, char * param, char * value)
{
    int status = mpv_set_option_string (ctx, param, value);

    if (status < 0)
    {
        logError ("mpv_set_option_string: %s = %s [KO]", param, value);
        logError ("%s", mpv_error_string (status));
        return (FAILURE);
    }
    else
    {
        logDebug ("mpv_set_option_string: %s = %s [OK]", param, value);
        return (SUCCESS);
    }
}

void sigPlay (int signal)
{
    // Set volume to the configuration value
    if (strlen (configuration.volume))
    {
        if (playerSetMpvOption (configuration.ctx, "volume", configuration.volume))
        {
            configuration.gStop = 1;
            return;
        }
    }

    // Set Play
    if (playerSetMpvOption (configuration.ctx, "pause", "no"))
    {
        configuration.gStop = 1;
        return;
    }
}

void sigPause (int signal)
{
    // Set Pause
    if (playerSetMpvOption (configuration.ctx, "pause", "yes"))
    {
        configuration.gStop = 1;
        return;
    }
}
