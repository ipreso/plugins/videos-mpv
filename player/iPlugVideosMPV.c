// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * Description: Manage videos for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Date:        $Date$
 *****************************************************************************/

#include "iPlugVideosMPV.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>
#include <signal.h>
#include <sys/types.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    iDebug ("[%s] initialization", PLUGINNAME);
    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char tmpBuffer [CMDLINE_MAXLENGTH];
    char filename [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    char param [32];
    char value [32];
    char opt[32];
    int posFilename, i;

    char optionAspect [32];
    char optionSeek [32];
    char optionDuration [32];
    char optionVolume [32];
    char optionRandom [32];
    char optionGeometry [32];

    iDebug ("[%s] get command", PLUGINNAME);

    memset (tmpBuffer, '\0', sizeof (tmpBuffer));
    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (opt, '\0', sizeof (opt));
    memset (optionAspect, '\0', sizeof (optionAspect));
    memset (optionSeek, '\0', sizeof (optionSeek));
    memset (optionDuration, '\0', sizeof (optionDuration));
    memset (optionVolume, '\0', sizeof (optionVolume));
    memset (optionRandom, '\0', sizeof (optionRandom));
    memset (optionGeometry, '\0', sizeof (optionGeometry));

    // Get the "aspect" parameter
    if (getParam (config, media, "aspect", value, sizeof (value)))
        strncpy (optionAspect, value, sizeof (optionAspect) - 1);

    // Get the "seek=X" parameter
    if (getParam (config, media, "seek", value, sizeof (value)))
    {
        if (atoi (value) != 0)
        strncpy (optionSeek, value, sizeof (optionSeek) - 1);
    }

    // Get the "volume=X" parameter
    if (getParam (config, media, "volume", value, sizeof (value)))
        strncpy (optionVolume, value, sizeof (optionVolume) - 1);

    // Get the "random=X" parameter
    if (getParam (config, media, "rdm", value, sizeof (value)))
        strncpy (optionRandom, value, sizeof (optionRandom) - 1);

    // Get the "duration=X" parameter
    if (getParam (config, media, "duration", value, sizeof (value)))
    {
        if (atoi (value) != 0)
            strncpy (optionDuration, value, sizeof (optionDuration) - 1);
    }


    // Get the filename
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (media) &&
                    media [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = media [posFilename];
    }

    // Check the file exists
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);
    if (access (fullPath, R_OK))
    {
        iError ("[%s][%s] File '%s' is not readable.", PLUGINNAME, zone, fullPath);
        iDebug ("[%s] path_playlists: %s", PLUGINNAME, config->path_playlists);
        iDebug ("[%s] currentProgram.name: %s", PLUGINNAME, config->currentProgram.name);
        iDebug ("[%s] playlist_media: %s", PLUGINNAME, config->playlist_media);
        return (NULL);
    }

    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, "%s %s "                          \
                                "--pid \"/tmp/videos.%s\" "     \
                                "--height %d "                  \
                                "--width %d "                   \
                                "--file \"%s\"",
                PLUGINVIDEO_APP,
                PLUGIN_DEFAULT_OPTIONS,
                optionRandom,
                height, width,
                fullPath);

    if (strlen (optionDuration))
    {
        snprintf (tmpBuffer, size-1, "%s --length %s", buffer, optionDuration);
        strncpy (buffer, tmpBuffer, size);
    }
    if (strlen (optionAspect))
    {
        snprintf (tmpBuffer, size-1, "%s --aspect %s", buffer, optionAspect);
        strncpy (buffer, tmpBuffer, size);
    }
    if (strlen (optionSeek))
    {
        snprintf (tmpBuffer, size-1, "%s --start %s", buffer, optionSeek);
        strncpy (buffer, tmpBuffer, size);
    }
    if (strlen (optionVolume))
    {
        snprintf (tmpBuffer, size-1, "%s --volume %s", buffer, optionVolume);
        strncpy (buffer, tmpBuffer, size);
    }
    if (strlen (buffer) + strlen (" &>/dev/null &") < size)
        strcat (buffer, " &>/dev/null &");

    iDebug ("[%s][%s] Command: %s", PLUGINNAME, zone, buffer);
    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;
    char buffer [CMDLINE_MAXLENGTH];

    iDebug ("[%s] preparation for WID %d", PLUGINNAME, wid);

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s][%s] Cannot get application information to prepare it.",
                PLUGINNAME, app.zone);
        return (0);
    }

    // Set the correct position for the Video window
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    iDebug ("[%s] %d - %s", PLUGINNAME, wid, buffer);
    system (buffer);

    return (1);
}

int play (sConfig * config, int wid)
{
    sApp app;
    char value [32];
    int duration;
    int pid;

    iDebug ("[%s] play for WID %d", PLUGINNAME, wid);

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information for WID %d.", PLUGINNAME, wid);
        return (0);
    }

    // Get PID of the running process
    pid = getPID (config, wid);
    if (!pid)
    {
        iError ("[%s][%s] Cannot play video for WID %d", PLUGINNAME, app.zone, wid);
        return (0);
    }

    // Send signal to play the Video
    kill (pid, SIGUSR1);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
    {
        iDebug ("[%s][%s] Cannot get video's duration.", PLUGINNAME, app.zone);
        duration = 0;
    }
    else
    {
        duration = atoi (value);

        // Apply the pre-cut to switch to next media before end of the video
        if (duration > 0 && 
                PLUGIN_DEFAULT_PRECUT > 0 && 
                duration > PLUGIN_DEFAULT_PRECUT)
            duration -= PLUGIN_DEFAULT_PRECUT;

        iDebug ("[%s][%s] Sleep duration: %ds", PLUGINNAME, app.zone, duration);
    }

    if (duration > 0)
        sleep (duration);
    else
    {
        // Check PID every seconds
        while (getPID (config, wid) > 0)
            sleep (1);
    }

    return (1);
}

int stop (sConfig * config, int wid)
{
    sApp app;
    int pid;

    iDebug ("[%s] stop for WID %d", PLUGINNAME, wid);
    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information for WID %d.", PLUGINNAME, wid);
        return (0);
    }

    // Get PID of the running process
    pid = getPID (config, wid);
    if (!pid)
    {
        // Video already ended
        return (0);
    }

    // Send TERMINATION signal to the process
    kill (pid, SIGTERM);
    return (0);
}

int clean ()
{
    iDebug ("[%s] Killing all instances of %s", PLUGINNAME, PLUGINVIDEO_APP);

    system ("pkill iVideosPlayer");
    return (1);
}

/*****************************************************************************/
// Specific functions for this plugin
int getPID (sConfig * config, int wid)
{
    sApp app;
    char optionRandom [32];
    FILE * f = NULL;
    char pidfile [FILENAME_MAXLENGTH];
    int current_pid = 0;

    memset (optionRandom, '\0', sizeof (optionRandom));
    memset (pidfile, '\0', sizeof (pidfile));

    // Get PID written by the iVideosPlayer process, thanks to the 'rdm' parameter
    // - Get App info in shared memory
    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information for WID %d.", PLUGINNAME, wid);
        return (0);
    }
    // - Get Random Parameter
    if (!getParam (config, app.item, "rdm", optionRandom, sizeof (optionRandom)))
    {
        iError ("[%s][%s] Cannot get Random parameter for WID %d", PLUGINNAME, app.zone, wid);
        return (0);
    }

    // - Get Content of PID file
    snprintf (pidfile, sizeof (pidfile) - 1, "/tmp/videos.%s", optionRandom);
    f = fopen (pidfile, "r");
    if (!f)
    {
        iError ("[%s][%s] Cannot get PID file for WID %d", PLUGINNAME, app.zone, wid);
        return (0);
    }
    if (fscanf (f, "%d", &current_pid) != 1)
    {
        iError ("[%s][%s] Cannot get PID from file '/tmp/videos.%s' for WID %d",
                    PLUGINNAME, app.zone, optionRandom, wid);
        fclose (f);
        return (0);
    }
    return (current_pid);
}
