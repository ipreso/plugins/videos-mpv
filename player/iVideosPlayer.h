// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * Description: Binary managing Videos, using libmpv
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Date:        $Date$
 *****************************************************************************/

// Define return values
#define SUCCESS 0
#define FAILURE 1

// Define Video API to use
#define RENDER_DEFAULT 0
#define RENDER_VAAPI   1
#define RENDER_VDPAU   2

// Other defines
#define PLAYERNAME      "iVideosPlayer"
#define DEFAULT_PID_PATH    "/var/run/"PLAYERNAME

#define PATH_MAX_LENGTH     1024
#define TIME_MAX_LENGTH     15
#define ASPECT_MAX_LENGTH   15
#define VOLUME_MAX_LENGTH   4


#define HELP ""                                                         \
"Usage:\n"                                                              \
PLAYERNAME" [options]\n"                                                \
"  <options>:\n"                                                        \
"    --aspect scale           Show video by scaling it to the window\n" \
"             keep-crop                     keeping its aspect but cutting videos part to fit the window\n" \
"             keep-resize                   keeping its aspect but downsizing it to fit the window\n" \
"    -h, --help               Show this helpful message\n"              \
"    --file X                 Play file 'X'\n"                          \
"    --height X               Set window height to X pixels\n"          \
"    --length X               Stop after X seconds'\n"                  \
"    --pause                  Start video in 'pause'\n"                 \
"    --pid X                  Write the PID of the running process to file 'X'\n" \
"    --start X                Seek to X seconds\n"                      \
"    --vaapi                  Use Video Acceleration API to render video\n" \
"    --vdpau                  Use Video Decode and Presentation API for UNIX render video\n" \
"    --width X                Set window width to X pixels\n"           \
"    --x-axis X               Set window at X pixels of the left border\n" \
"    --y-axis X               Set window at X pixels of the top border\n" \
"\n"                                                                    \
"Signals:\n"                                                            \
"    - SIGUSR1                Set the volume correctly and play the video\n" \
"    - SIGUSR2                Set the volume to 0 and pause the video\n" \
"\n"



// Standard
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>

// Use 'libmvp', the API library for mpv player.
#include <mpv/client.h>

// Parameters parsing
#include <getopt.h>

int configureLogs ();
int initConfiguration ();
int parseParameters (int argc, char ** argv);

int cleanExit               (int returnValue);

void logDebug               (char * message, ...);
void logInfo                (char * message, ...);
void logError               (char * message, ...);

int writePidFile            (char * file);

int playerLoad              (char * file);
int playerSetInitOptions    (mpv_handle * ctx);
int playerSetMpvOption      (mpv_handle * ctx, char * param, char * value);

void sigPlay                (int signal);
void sigPause               (int signal);

typedef struct s_conf
{
    // PID file
    char pidfile[PATH_MAX_LENGTH];

    // File to play
    char file   [PATH_MAX_LENGTH];

    // Play options
    char aspect [ASPECT_MAX_LENGTH];
    char length [TIME_MAX_LENGTH];
    char start  [TIME_MAX_LENGTH];
    char volume [VOLUME_MAX_LENGTH];
    int pause;
    int render;

    // Geomery
    int x;
    int y;
    int width;
    int height;

    mpv_handle * ctx;

    // Stop video play and exit
    int gStop;

} sConf;

static struct option long_options[] =
{
    {"aspect",      required_argument,  0, 'a'},
    {"file",        required_argument,  0, 'f'},
    {"length",      required_argument,  0, 'l'},
    {"pause",       no_argument,        0, 'p'},
    {"pid",         required_argument,  0, 'P'},
    {"start",       required_argument,  0, 's'},
    {"volume",      required_argument,  0, 'v'},
    {"vaapi",       no_argument,        0, 'y'},
    {"vdpau",       no_argument,        0, 'z'},

    {"width",       required_argument,  0, 'W'},
    {"height",      required_argument,  0, 'H'},
    {"x-axis",      required_argument,  0, 'X'},
    {"y-axis",      required_argument,  0, 'Y'},

    {"help",        no_argument,        0, 'h'},

    {0, 0, 0, 0}
};
